package my.stabbingFurries.GGWP;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.loader.content.AsyncTaskLoader;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity {

    public static final String SERVERHOSTNAME = "https://atheroxportfolio.000webhostapp.com";
//    public static final String SERVERHOSTNAME = "http://virtual412.ies-sabadell.cat/virtual412/Niko";

    public static UserDB userDB;
    public static SQLiteDatabase db;
    SoundManager soundManager;
    private AlertDialog.Builder ctxSearch;
    public static Resources res;
    private String username;
    HttpURLConnection con;

    public User userSearched = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        userDB = new UserDB(this, "User", null, 1);
        db = userDB.getWritableDatabase();

        soundManager = new SoundManager(this);
        soundManager.setVol(200);
        Configuration config = getResources().getConfiguration();
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        res = new Resources(getAssets(),metrics,config);


        Button btnMainMenuSearch = (Button) findViewById(R.id.btnMainMenuSearch);
        btnMainMenuSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                soundManager.play(SoundLibrary.boton2);
                createCTXMainMenu().show();
            }
        });

    }

    private Dialog createCTXMainMenu(){
        LayoutInflater li = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View ctxInflate = li.inflate(R.layout.mainmenu_ctx_search,null);
        ctxSearch = new AlertDialog.Builder(this);
        ctxSearch.setTitle(res.getString(R.string.MainMenuCTXTitle));
        final EditText t = (EditText) ctxInflate.findViewById(R.id.txtMainMenuCTXUsername);
        ctxSearch.setView(ctxInflate);
        ctxSearch.setNegativeButton(res.getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                soundManager.play(SoundLibrary.boton1);
                dialog.cancel();
            }
        });
        ctxSearch.setPositiveButton(res.getString(R.string.accept), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                soundManager.play(SoundLibrary.boton2);
                getUsername(t.getText().toString());
            }
        });
        return ctxSearch.create();
    }

    public void getUsername(String username) {
        this.username = username;
        System.out.println("---------=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=--=-=-=-="+username+" "+this.username);
//        Toast.makeText(getApplicationContext(),username,Toast.LENGTH_LONG).show();

        //Buscar en la base local
        boolean heEncontradoEnLocal = false;
        Cursor searchPlayer = db.rawQuery("SELECT username, level, exp, icon, date FROM User WHERE username like ?",new String[]{username});
        if (searchPlayer != null && searchPlayer.moveToFirst()) {
            try {
                do {
                    userSearched = new User(
                            searchPlayer.getString(0),
                            searchPlayer.getInt(1),
                            searchPlayer.getInt(2),
                            searchPlayer.getString(3),
                            searchPlayer.getLong(4)
                    );
                    heEncontradoEnLocal = true;
                } while (searchPlayer.moveToNext());
            }catch (Exception e){

            }
        }

        if(userSearched != null && heEncontradoEnLocal) {
            System.out.println("------------------------------------------------------------- OLE OLE Base local");
            IrAUsuario();
            return;
        }


        //En caso de no encontrar en la base de datos local buscar en la general
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ninfo = cm.getActiveNetworkInfo();

        if(ninfo != null){
            if(ninfo.isConnected()){
                try{
                    System.out.println("--------------------------------------------A BUSCAR");
                    new JsonTask(this).execute(new URL(SERVERHOSTNAME+"/get.php?name="+username));
                    System.out.println("--------------------------------------------BUSCAO");
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            }

        }
    }

    public class JsonTask extends AsyncTask<URL,Void, User>{

        private ProgressDialog pd;

        public JsonTask(Activity activity){
            pd = new ProgressDialog(activity);
        }

        @Override
        protected void onPreExecute(){
            pd.setMessage("Searching player...");
            pd.setIndeterminate(false);
            pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected User doInBackground(URL... urls) {
            User user = null;
            try {
                con = (HttpURLConnection) urls[0].openConnection();
                con.setConnectTimeout(10000);
                con.setReadTimeout(10000);

                int state = con.getResponseCode();
                if(state == 200){
                    System.out.println("-------------------------------------------- pillar el input");

                    InputStream in = new BufferedInputStream(con.getInputStream());
                    user = GsonUserParser.read(in);
                    System.out.println("--------------------------------------------tengo user");

                }
            }catch (Exception e){
                System.err.println("Unpareseable, usuario inexistente");
            }finally {
                con.disconnect();
            }
            return user;
        }

        @Override
        protected void onPostExecute(User user) {
            System.out.println("--------------------------------------------ACABADO");

            if(pd.isShowing())
                pd.dismiss();
            if(user !=null){
                userSearched = user;

                System.out.println(new Timestamp(System.currentTimeMillis()).getTime());
                System.out.println(new Timestamp(System.currentTimeMillis()));

                ContentValues nuevoRegistro = new ContentValues();
                nuevoRegistro.put("username", user.getUsername());
                nuevoRegistro.put("level", user.getLevel());
                nuevoRegistro.put("exp", user.getExp());
                nuevoRegistro.put("icon", user.getIcon());
                nuevoRegistro.put("date", user.getDate().getTime());

                boolean heEncontradoEnLocal = false;
                Cursor searchPlayer = db.rawQuery("SELECT * FROM User WHERE username like ?",new String[]{user.getUsername()});
                if (searchPlayer != null && searchPlayer.moveToFirst()) {
                    try {
                        do {
                            heEncontradoEnLocal = true;
                        } while (searchPlayer.moveToNext());
                    }catch (Exception e){}
                }
                System.out.println(searchPlayer);
                if(heEncontradoEnLocal) {
                    db.update("User", nuevoRegistro, "username like ?", new String[]{user.getUsername()});
                    System.out.println("Actualizado");
                }else {
                    db.insert("User", null, nuevoRegistro);
                    System.out.println("Creado");
                }

                IrAUsuario();
            }else{
                Toast.makeText(getApplicationContext(),"El usuario no existe",Toast.LENGTH_LONG).show();
            }
        }
    }

    private void IrAUsuario() {
        Intent i = new Intent(getApplicationContext(), SearchPlayer.class);
        Bundle bundle = new Bundle();

        bundle.putString("username",userSearched.getUsername());
        bundle.putInt("level",userSearched.getLevel());
        bundle.putInt("exp",userSearched.getExp());
        bundle.putString("icon",userSearched.getIcon());
        bundle.putLong("date", userSearched.getDate().getTime());

        i.putExtras(bundle);
        userSearched = null;
        startActivityForResult(i,1);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        System.out.println("Ole "+requestCode);
        if(requestCode == 1) {
            if (resultCode == RESULT_OK) {
                String n = data.getStringExtra("username");
                System.out.println("Ole2 " + n);
                if (n == null || n == "")
                    return;


                ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo ninfo = cm.getActiveNetworkInfo();

                if (ninfo != null) {
                    if (ninfo.isConnected()) {
                        try {
                            System.out.println("--------------------------------------------A BUSCAR");
                            new JsonTask(this).execute(new URL(SERVERHOSTNAME + "/get.php?name=" + n));
                            System.out.println("--------------------------------------------BUSCAO");
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }
}