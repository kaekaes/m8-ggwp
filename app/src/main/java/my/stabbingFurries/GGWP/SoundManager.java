package my.stabbingFurries.GGWP;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;

public class SoundManager {

    private Context context;
    private SoundPool soundPool;
    private float rate          = 1;
    private float masterVolume  = 1;
    private float leftVol       = 1;
    private float rightVol      = 1;
    private float balance       = 1;

    public SoundManager(Context appContext){
        this.context = appContext;
        soundPool = new SoundPool(16, AudioManager.STREAM_MUSIC, 100);
        SoundLibrary.boton1 = load(R.raw.maincra);
        SoundLibrary.boton2 = load(R.raw.buton2);
    }

    public int load(int sound_id){
        return soundPool.load(context, sound_id, 1);
    }

    public void play(int sound_id){
        soundPool.play(sound_id, leftVol, rightVol, 1, 0, rate);
    }

    public void setVol(float vol){
        masterVolume = vol;
        if(balance < 1f) {
            leftVol = masterVolume;
            rightVol = masterVolume * balance;
        }else{
            rightVol = masterVolume;
            leftVol = masterVolume * (2-balance);
        }
    }

    public void setSpeed(float speed){
        rate = speed;
        if(rate<.01f)
            rate = 0.01f;

        if(rate > 2)
            rate = 2;
    }

    public void setBalance(float balVal){
        balance = balVal;
        setVol(masterVolume);
    }

    public void dispose(){
        soundPool.release();
    }
}
