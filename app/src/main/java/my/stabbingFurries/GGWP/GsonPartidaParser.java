package my.stabbingFurries.GGWP;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class GsonPartidaParser {

    public static Partidas read(InputStream in) throws IOException{
        Gson gson = new Gson();

        JsonReader reader = new JsonReader(new InputStreamReader(in,"UTF-8"));

        Partidas partidas = gson.fromJson(reader,Partidas.class);
        System.out.println(partidas);
        reader.close();
        System.out.println("--------------------------------------------devolver");

        return partidas;
    }
}
