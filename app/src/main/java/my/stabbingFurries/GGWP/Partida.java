package my.stabbingFurries.GGWP;

import java.sql.Timestamp;

public class Partida {

    private int idJugador;
    private int idPartida;
    private String personaje;
    private int kills;
    private int deaths;
    private int assists;
    private int creeps;
    private String team;

    private String map;
    private String winTeam;
    private String startTime;
    private String endTime;

    public Partida(int idJugador, int idPartida, String personaje, int kills, int deaths, int assists, int creeps, String team, String map, String winTeam, String startTime, String endTime) {
        this.idJugador = idJugador;
        this.idPartida = idPartida;
        this.personaje = personaje;
        this.kills = kills;
        this.deaths = deaths;
        this.assists = assists;
        this.creeps = creeps;
        this.team = team;
        this.map = map;
        this.winTeam = winTeam;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public int getIdJugador() {
        return idJugador;
    }

    public int getIdPartida() {
        return idPartida;
    }

    public String getPersonaje() {
        return personaje;
    }

    public int getKills() {
        return kills;
    }

    public int getDeaths() {
        return deaths;
    }

    public int getAssists() {
        return assists;
    }

    public int getCreeps() {
        return creeps;
    }

    public String getTeam() {
        return team;
    }

    public String getMap() {
        return map;
    }

    public String getWinTeam() {
        return winTeam;
    }

    public String getStartTime() {
        return startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setIdJugador(int idJugador) {
        this.idJugador = idJugador;
    }

    public void setIdPartida(int idPartida) {
        this.idPartida = idPartida;
    }

    public void setPersonaje(String personaje) {
        this.personaje = personaje;
    }

    public void setKills(int kills) {
        this.kills = kills;
    }

    public void setDeaths(int deaths) {
        this.deaths = deaths;
    }

    public void setAssists(int assists) {
        this.assists = assists;
    }

    public void setCreeps(int creeps) {
        this.creeps = creeps;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public void setMap(String map) {
        this.map = map;
    }

    public void setWinTeam(String winTeam) {
        this.winTeam = winTeam;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    @Override
    public String toString() {
        return "\nPartida{" +
                "idJugador=" + idJugador +
                ", idPartida=" + idPartida +
                ", personaje='" + personaje + '\'' +
                ", kills=" + kills +
                ", deaths=" + deaths +
                ", assists=" + assists +
                ", creeps=" + creeps +
                ", team='" + team + '\'' +
                ", map='" + map + '\'' +
                ", winTeam='" + winTeam + '\'' +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                '}';
    }
}
