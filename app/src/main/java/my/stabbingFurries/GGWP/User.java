package my.stabbingFurries.GGWP;

import java.sql.Timestamp;
import java.util.Date;

public class User {

    private String username;
    private int level;
    private int exp;
    private String icon;
    private Timestamp date;

    public User(String username, int level, int exp, String icon) {
        this.username = username;
        this.level = level;
        this.exp = exp;
        this.icon = icon;
        try {
            this.date = new Timestamp(new Date().getTime());
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public User(String username, int level, int exp, String icon, long date) {
        this.username = username;
        this.level = level;
        this.exp = exp;
        this.icon = icon;
        try {
            this.date = new Timestamp(date);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public String toString(){
        return username+" "+level+" "+exp+" "+icon+" "+date;
    }

    public int getLevel() {
        return level;
    }

    public int getExp() {
        return exp;
    }

    public String getIcon() {
        return icon;
    }

    public String getUsername() {
        return username;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(){
        try {
            this.date = new Timestamp(new Date().getTime());
        }catch (Exception e){
            e.printStackTrace();
        }    }
}
